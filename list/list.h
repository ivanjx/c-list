#ifndef LIST_H
#define LIST_H

typedef void * VOID_P;

typedef struct LIST_NODE {
  VOID_P data; /* Data the node holds. */
  struct LIST_NODE * prev; /* Previous node. */
  struct LIST_NODE * next; /* Next node. */
} LIST_NODE;

typedef LIST_NODE * LIST_NODE_P;

typedef struct LIST {
  LIST_NODE_P first; /* The first node. */
  LIST_NODE_P last; /* The last node. */
  int size; /* List size. */
} LIST;

typedef LIST * LIST_P;

LIST_P list_init();
LIST_NODE_P list_get_node(LIST_P list, int index);
int list_add(LIST_P list, VOID_P data);
int list_insert(LIST_P list, VOID_P data, int index);
VOID_P list_get_data(LIST_P list, int index);
int list_remove(LIST_P list, int index);
int list_clear(LIST_P list);
int list_destroy(LIST_P list);

#endif