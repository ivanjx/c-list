#include <stdlib.h>
#include <stdio.h>
#include "list.h"

LIST_P list_init() {
  /* Allocating memory. */
  LIST_P list = (LIST_P)malloc(sizeof(LIST));

  if(list == NULL) {
    printf("Memory allocation error\n");
    return NULL;
  }

  /* Initial values. */
  list->size = 0;
  list->first = NULL;
  list->last = NULL;

  /* Done. */
  return list;
}

int list_add(LIST_P list, VOID_P data) {
  // Inserting to the last index + 1.
  int index = list->size;
  return list_insert(list, data, index);
}

LIST_NODE_P list_get_node(LIST_P list, int index) {
  // Checking index.
  if(index + 1 > list->size) {
    return NULL;
  }

  if(list->first == NULL) {
    return NULL;
  }

  // Getting target index node by looping.
  LIST_NODE_P node = list->first;
  int currIndex = 0;

  while(currIndex != index) {
    // Getting next node.
    node = node->next;
    currIndex += 1;
  }

  // Done.
  return node;
}

int list_insert(LIST_P list, VOID_P data, int index) {
  // Checking index.
  if(index < 0) {
    printf("Invalid index\n");
    return -1;
  }

  if(index > list->size) {
    printf("Index out of bounds\n");
    return -1;
  }

  // Creating new node.
  LIST_NODE_P node = (LIST_NODE_P)malloc(sizeof(LIST_NODE));
  node->data = data;
  node->next = NULL;
  node->prev = NULL;

  // Getting target node to be replaced.
  LIST_NODE_P oldNode = list_get_node(list, index);

  if(oldNode == NULL) {
    // Possible cause of null old node:
    // Very first.
    // Very last.

    // Check if last is null.
    if(list->last == NULL) {
      // The first item.
      list->first = node;
      list->last = node;
    } else {
      // Get last node.
      LIST_NODE_P last = list->last;

      // Append.
      list->last->next = node;
      node->prev = list->last;
      list->last = node;
    }
  } else {
    // Updating new node state with old node's.
    node->prev = oldNode->prev;
    node->next = oldNode;

    // Updating old node previous pointer.
    if(oldNode->prev != NULL) {
      oldNode->prev->next = node;
    }

    oldNode->prev = node;

    // Updating list's first.
    if(index == 0) {
      list->first = node;
    }

    // Updating list's last.
    if(index == list->size - 1) {
      list->last = node;
    }
  }

  // Updating list size.
  list->size += 1;
}

VOID_P list_get_data(LIST_P list, int index) {
  // Checking index because the function below doesnt implement one.
  if(index < 0) {
    printf("Invalid index\n");
    return NULL;
  }

  if(index + 1 > list->size) {
    printf("Index out of bounds\n");
    return NULL;
  }

  // Getting node.
  LIST_NODE_P node = list_get_node(list, index);

  // Done.
  return node->data;
}

int list_remove(LIST_P list, int index) {
  // Validating index.
  if(index < 0) {
    printf("Invalid index\n");
    return -1;
  }

  if(index + 1 > list->size) {
    printf("Index out of bounds\n");
    return -1;
  }

  // Getting list node.
  LIST_NODE_P node = list_get_node(list, index);

  // Rewiring.
  if(node->prev != NULL) {
    node->prev->next = node->next;
  } else {
    // If prev is null, this must be the first.
    list->first = node->next;
  }

  if(index + 1 == list->size) {
    list->last = node->prev;
  }

  // Deallocating.
  free(node);

  // Updating list size.
  list->size -= 1;

  // Removing possible random memory address.
  if(list->size == 0) {
    list->first = NULL;
    list->last = NULL;
  }

  // Done.
  return 0;
}

int list_clear(LIST_P list) {
  if(list->size == 0) {
    return 0;
  }

  while(list->size > 0) {
    int r = list_remove(list, 0);

    if(r != 0) {
      return r;
    }
  }

  return 0;
}

int list_destroy(LIST_P list) {
  /* Checking the list size. */
  if(list->size > 0) {
    printf("List is not empty");
    return -1;
  }

  /* Deallocating memory. */
  free(list);

  /* Done. */
  return 0;
}