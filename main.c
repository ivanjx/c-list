#include <stdio.h>
#include "list/list.h"

void print_list(LIST_P list) {
  for(int i = 0; i < list->size; ++i) {
    // Getting item.
    int * storedX = (int *)list_get_data(list, i);
    printf("Index %d value address: %x\n", i, storedX);
    printf("Index %d value: %d\n", i, *storedX);
  }
}

int main() {
  // Allocate.
  LIST_P list = list_init();

  // Adding item.
  int x = 100;
  list_add(list, &x);

  int y = 200;
  list_add(list, &y);

  int z = 300;
  list_add(list, &z);

  int a = 1000;
  list_insert(list, &a, 0);

  print_list(list);

  // Clearing.
  list_clear(list);

  // Deallocate.
  list_destroy(list);

  return 0;
}